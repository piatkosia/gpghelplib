﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyOwnGPGLib;

namespace SprawdzenieDecryptu
{
    class Program
    {
        static void Main(string[] args)
        {
            MyOwnGPGLib.GpgCrypt.exepath = @"C:\Program Files (x86)\GNU\GnuPG\gpg2.exe";
            string tmppasswd  = args[0];
            EASpack pack1 = new EASpack
            {
                InputFile = @"C:\Users\piatkosia\Desktop\plaintekscik.txt", //plain
                OutputFile = @"C:\Users\piatkosia\Desktop\testujemy_program.txt", //szyfrogram
                passwd = CmdApi.ToSecureString(tmppasswd),
                KeyId = "piatkosia.apt@interia.pl",
                rcpt = "piatkosia.apt@interia.pl"
            };
            GpgCrypt.EncryptCardAndSignHash(pack1);
            DECpack pack2 = new DECpack
            {
                OutputFilePath = @"C:\Users\piatkosia\Desktop\Czy_to_samo.txt",
                passwd = CmdApi.ToSecureString(tmppasswd),
                signerShortId = "82F1BB24"
            };
            GpgCrypt.DecryptWhenSignedHash(@"C:\Users\piatkosia\Desktop\testujemy_program.txt", pack2);
        }
    }
}
