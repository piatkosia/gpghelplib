﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using MyOwnGPGLib;

namespace ExampleForLib
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GpgCrypt.exepath = @"C:\Program Files (x86)\GNU\GnuPG\gpg2.exe";
            
        }

        private void Run()
        {
            DateTime tmpdate;
            if (cal1.SelectedDate.HasValue)
            {
                tmpdate = cal1.SelectedDate.Value;
            }
            else tmpdate = DateTime.Now;
            SigningPack sgn = new SigningPack()
            {
                Passwd = pass.SecurePassword,
                DateOfVote = tmpdate,
                Originator = who.Text,
                rcpt = who.Text
            };
            Stream str = GpgCrypt.SetSignedDate(sgn);
            str.Position = 0;
            using (StreamReader reader = new StreamReader(str, Encoding.UTF8))
            {
                using (var fileStream = File.Create(@"e:\testsign.gpg"))
                {
                    str.Seek(0, SeekOrigin.Begin);
                    str.CopyTo(fileStream);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Run();
        }
    }
}
