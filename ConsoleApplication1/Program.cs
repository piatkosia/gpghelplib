﻿using System;
using System.IO;
using MyOwnGPGLib;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string PathToCheck = "E:\\testsign.gpg";
            GpgCrypt.exepath = @"C:\Program Files (x86)\GNU\GnuPG\gpg2.exe";
            if (File.Exists(PathToCheck) == false)
            {
                Console.WriteLine("Miało sprawdzić podpisany plik " + PathToCheck + " a go za cholerę nie widzę.");
            }

            VerifyOutput test = GpgCrypt.VerifySignedFile(PathToCheck);
            try
            {
                Console.WriteLine(test.Id + "\n" + test.When + "\n" + test.goodSignature + "\n" + test.msg);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                
            }
            Console.ReadKey();
        }
    }
}
