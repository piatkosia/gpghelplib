﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnGPGLib
{
    public class DECpack
    {
        public string signerShortId { get; set; }

        public System.Security.SecureString passwd { get; set; }

        public string OutputFilePath { get; set; }
    }
}
