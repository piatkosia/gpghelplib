﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace MyOwnGPGLib
{
    public class CmdApi
    {
        /// <summary>
        /// Wywołuje komendę w CMD bezpośrednio w apce
        /// przykładowe użycie: DoCMDCommand("notepad.exe", "plik.txt", @"c:/jakas/sciezka/", out zmienna)
        /// </summary>
        /// <param name="command"> nazwa programu (lub jego ścieżka)</param>
        /// <param name="paras">nazwa parametrów programu</param>
        /// <param name="workdir">folder roboczy</param>
        /// <param name="cmdoutput">zmienna wyjściowa, reprezentująca wyjście z konsoli </param>
        private static void DoCMDCommand(string command, string paras, string workdir, out string cmdoutput)
        {
            Process pProcess = new Process {StartInfo =
            {
                CreateNoWindow = true, 
                FileName = command,
                Arguments = paras,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                WorkingDirectory = workdir
         
            }};
            pProcess.Start();
            cmdoutput = pProcess.StandardOutput.ReadToEnd();
            pProcess.WaitForExit();
            pProcess.Close();
        }

        static byte[] entropy = Encoding.Unicode.GetBytes("Tu moze byc zupelnie cokolwiek.");

        /// <summary>
        /// Funkcja używana do zaszyfrowania haseł usera jak uzna za konieczne zapisanie go w konfiguracji programu
        /// </summary>
        /// <example>
        /// AppSettings.Password = ConfigEncryptString(ToSecureString(PasswordTextBox.Password));
        /// czy 
        /// Properties.Settings.Default.SvrPasswd = ConfigEncryptString(ToSecureString(PasswordTextBox.Password));
        /// </example>
        /// <param name="input">hasło do zaszyfrowania</param>
        /// <returns>hasło zaszyfrowane</returns>
        public static string ConfigEncryptString(SecureString input)
        {
            byte[] encryptedData = ProtectedData.Protect(
                Encoding.Unicode.GetBytes(ToInsecureString(input)),
                entropy,
                DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        /// <summary>
        /// Funkcja zwraca zaszyfrowane hasła, zaszyfrowane funkcją ConfigEncryptString
        /// </summary>
        /// <example>SecureString password = DecryptString(Properties.Settings.Default.Password)</example>
        /// <param name="encryptedData"> dane zaszyfrowane</param>
        /// <returns>gotowe hasło typu secure string</returns>
        public static SecureString ConfigDecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy,
                    DataProtectionScope.CurrentUser);
                return ToSecureString(Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        /// <summary>
        /// Zwraca secure stringa ze zwykłego stringa
        /// </summary>
        /// <param name="input">ciąg znaków typu string</param>
        /// <returns>ciąg znaków typu SecureString</returns>
        public static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        /// <summary>
        /// Ściąga secure ze stringa:)
        /// </summary>
        /// <param name="input">ciąg znaków typu SecureString</param>
        /// <returns>ciąg znaków typu string</returns>
        public static string ToInsecureString(SecureString input)
        {
            string returnValue = string.Empty;
            IntPtr ptr = Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }
        /// <summary>
        /// Zwraca ciąg znaków ze strumienia
        /// </summary>
        /// <param name="s">Strumień</param>
        /// <returns>ciąg znaków</returns>
        public static string FromStream(Stream s)
        {
            StreamReader reader = new StreamReader(s);
            return reader.ReadToEnd();
        }
        /// Zwraca strumień z ciągu znaków
        /// </summary>
        /// <param name="s">ciąg znaków</param>
        /// <returns>strumień</returns>
        public static Stream ToStream(string s)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(s);
            return new MemoryStream(byteArray);
        }
        /// <summary>
        /// Tworzy ścieżkę do pliku tymczasowego
        /// </summary>
        /// <param name="ext">rozszerzenie</param>
        /// <example>string path =  GetUniqueTempFilename(".txt");</example>
        /// <returns>wygenerowana temp-ścieżka</returns>
        public static string GetUniqueTempFilename(string ext)
        {
            return System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() + ext;

        }
    }
}
