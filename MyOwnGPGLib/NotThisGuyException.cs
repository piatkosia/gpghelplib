﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnGPGLib
{

    [Serializable]
    public class NotThisGuyException : Exception
    {

        public NotThisGuyException()
        {
        }


        public NotThisGuyException(String message)
            : base(message)
        {
        }

        protected NotThisGuyException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public NotThisGuyException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}