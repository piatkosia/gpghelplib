﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnGPGLib
{

    [Serializable]
    public class NotSignedException : Exception
    {

        public NotSignedException()
        {
        }


        public NotSignedException(String message)
            : base(message)
        {
        }

        protected NotSignedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public NotSignedException(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}