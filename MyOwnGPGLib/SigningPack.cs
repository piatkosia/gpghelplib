﻿using System;
using System.IO;
using System.Security;
using System.Text;

namespace MyOwnGPGLib
{
    public class SigningPack
    {
        public SecureString Passwd { get; set; }

        public  string Originator { get; set; }

        public  string rcpt { get; set; }

        public DateTime DateOfVote { get; set; }
        public Stream DatePrepare(DateTime data)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(data.ToShortDateString() ?? ""));
        }
    }
}
