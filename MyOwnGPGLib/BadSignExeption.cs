﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnGPGLib
{

    [Serializable]
    public class BadSignExeption : Exception
    {

        public BadSignExeption()
        {
        }


        public BadSignExeption(String message)
            : base(message)
        {
        }

        protected BadSignExeption(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public BadSignExeption(String message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}