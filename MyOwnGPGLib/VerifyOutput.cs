﻿using System;

namespace MyOwnGPGLib
{
    public class VerifyOutput
    {
        public bool goodSignature { get; set; }
        public string Id { get; set; }

        public DateTime When { get; set; }

        public string msg { get; set; }
    }
}
