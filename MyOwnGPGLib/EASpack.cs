﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyOwnGPGLib
{
    public class EASpack
    {
        public Stream InputData { get; set; }


        public string KeyId { get; set; }

        public string rcpt { get; set; }

        public System.Security.SecureString passwd { get; set; }

        public string InputFile { get; set; }

        public string OutputFile { get; set; }
    }
}
