﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using GpgApi;
using PgpSharp;
using PgpSharp.GnuPG;

namespace MyOwnGPGLib
{
    public class GpgCrypt
    {
        public static string exepath;
        public static IPgpTool tool = new GnuPGTool(); 

        public void WindowedSetExePath()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Title = "Wybierz ścieżkę programu gpg/gpg2.";
            ofd.Filter = "pliki wykonywalne|*.exe";
            ofd.CheckFileExists = true;
            if (File.Exists(exepath))
            {
                ofd.FileName = Path.GetFileName(exepath);
                ofd.InitialDirectory = Path.GetDirectoryName(exepath);
            }
            var result = ofd.ShowDialog();
            if (result == DialogResult.OK) 
            {
                exepath = ofd.FileName;
            }
        }

        public static Stream SetSignedDate(SigningPack dataToPrepare)
        {
            GnuPGConfig.GnuPGExePath = exepath;
            StreamDataInput SignArg = new StreamDataInput()
            {
                Armor = true,
                InputData = dataToPrepare.DatePrepare(dataToPrepare.DateOfVote),
                Operation = DataOperation.ClearSign,
                Originator = dataToPrepare.Originator,
                Passphrase = dataToPrepare.Passwd,
                Recipient = dataToPrepare.rcpt
            };
            return tool.ProcessData(SignArg);
        }

        public static Stream SetSignedCard(SigningPack dataToPrepare, Stream card)
        {
            GnuPGConfig.GnuPGExePath = exepath;
            StreamDataInput SignArg = new StreamDataInput()
            {
                Armor = true,
                InputData = card,
                Operation = DataOperation.ClearSign,
                Originator = dataToPrepare.Originator,
                Passphrase = dataToPrepare.Passwd,
                Recipient = dataToPrepare.rcpt
            };
            return tool.ProcessData(SignArg);
        }

        /// <summary>
        /// Weryfikuje podpisany plik o podanej ścieżce. 
        /// </summary>
        /// <param name="path"></param>
        /// <returns>Zwraca informacje na temat podpisu. Przy niepodpisanych lub błędzie zwraca nulla.</returns>
        public static VerifyOutput VerifySignedFile(string path)
        {
            if (File.Exists(path)!= true)throw new FileNotFoundException("Brak pliku do zweryfikowania");
            VerifyOutput ret = new VerifyOutput();
            GpgInterface.ExePath = exepath;
            GpgVerifySignature verify = new GpgVerifySignature(path);
            var res = verify.Execute();
            Thread.Sleep(2000);
            if (verify.IsSigned == true)
            {
                if (verify.IsGoodSignature) ret.goodSignature = true;
                
                ret.Id = verify.SignatureKeyId.ToString();
                ret.When = verify.SignatureDateTime;
                ret.msg = res.Message.ToString();
                
                return ret;
               

            }
            else throw new NotSignedException(String.Format("Plik {0} nie jest prawidłowo podpisany",path));
        }

       /// <summary>
       /// Usuwa clearsigny i armory 
       /// </summary>
       /// <param name="inputFilePath">ścieżka do pliku</param>
       /// <returns>string z zawartością</returns>
        public static string DearmorFile(string inputFilePath)
       {
           string longstring = "";
           IEnumerable<string> WithoutHeader =  File.ReadAllLines(inputFilePath).Skip(3); //--begin cośtam, linijka z algorytmem i pusta linia
           foreach (string x in WithoutHeader)
           {
               if (x.Contains("-----BEGIN PGP SIGNATURE-----")) break;
               longstring += x + "\n";
           }
            return longstring;
        }

        //Pomyśleć, czy tu nie dać innej formy w przyszłości
        public static string EncryptCardAndSignHash(EASpack auth)
        {
            string tempfile = CmdApi.GetUniqueTempFilename(".asc.tmp");
            GnuPGConfig.GnuPGExePath = exepath;
            FileDataInput cryptArg = new FileDataInput()
            {
                Armor = true,
                InputFile = auth.InputFile,
                OutputFile = tempfile,
                Originator = auth.KeyId,
                Operation = DataOperation.Encrypt,
                Recipient = auth.rcpt,
                Passphrase = auth.passwd,
                
            };
            tool.ProcessData(cryptArg);
            cryptArg.InputFile = tempfile;
            cryptArg.OutputFile = auth.OutputFile;
            cryptArg.Operation = DataOperation.ClearSign;
            tool.ProcessData(cryptArg);
            return cryptArg.OutputFile;
        }
        /// <summary>
        /// Funkcja zwracająca odszyfrowaną zawartość podpisanego szyfrogramu
        /// </summary>
        /// <param name="path"></param>
        /// <param name="DECpack"></param>
        /// <returns></returns>
        public static string DecryptWhenSignedHash(string path, DECpack decpack)
        {
            if (File.Exists(path) != true) throw new FileNotFoundException(String.Format("Plik {0} nie istnieje. Nie ma co dekryptować."));
            VerifyOutput signOk = VerifySignedFile(path);
            string EncryptedString = DearmorFile(path);
            //Stream s = CmdApi.ToStream(EncryptedString);
            string EncryptedFileName = CmdApi.GetUniqueTempFilename(".gpg");  
            if (signOk.goodSignature == false) throw new BadSignExeption("Błędna sygnatura podpisu");
            if (signOk.Id.Contains(decpack.signerShortId) == false) throw new NotThisGuyException("Szyfrogram podpisany przez inną osobę/instytucję niż spodziewana");
            File.WriteAllText(EncryptedFileName, EncryptedString);
            string[] temp = File.ReadAllLines(EncryptedFileName);
            //Jak koleś poprawi liba (czyli wywali "- " ze stringu, wyrzucić to)
            temp[0] = temp[0].Remove(0, 2);
            temp[temp.Length - 1] = temp[temp.Length - 1].Remove(0, 2);
            File.Delete(EncryptedFileName);
            File.WriteAllLines(EncryptedFileName, temp);
          //wyrzucić do tąd
             GnuPGConfig.GnuPGExePath = exepath;
             var decryptArg = new FileDataInput
             {
                 InputFile = EncryptedFileName,
                 OutputFile = decpack.OutputFilePath,
                 Operation = DataOperation.Decrypt,
                 Passphrase = decpack.passwd,
             };
           
                tool.ProcessData(decryptArg);
            
            File.Delete(EncryptedFileName);
            return File.ReadAllText(decpack.OutputFilePath);

        }
    }
}
